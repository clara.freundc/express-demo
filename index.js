//importe Express
const express = require('express');
const app = express();
const password = "Order 66";
const warning = "YOU SHALL NOT PASS!"

//le port 3000 est utilisé ce qui donne cette adresse : http://localhost:3000
const port = 3000;

//va chercher, le chemin, response
//ensuite envoie le texte sur la page de base (racine)
app.get('/', (req, res) => {
    res.send('Bienvenue! Ici on apprend à cuisiner! Je suis avec mon amie Ponita la licorne! Coucou Ponita! Coucou tout le monde! Hihi...');
})
//une route GET qui répond avec un objet contenant du texte
app.get('/secret', (req, res) => {
    res.send("Ho non! Tu m'as découvert! Mais tu n'auras pas mon autre secret très très bien caché...")
})
//une route GET qui répond avec un objet
app.get('/verywellhiddensecret', (req, res) => {
    res.send({ name: 'plan secret très très bien caché', hidden: true, hiddenLevel: 9000, warning: 'YOU SHALL NOT PASS', goal: 'unlimited powaaa', password: "Order 66", hiddenTreasure: 42 })
})
//permet d'écouter les connexions au port et de lancer le serveur
app.listen(port, () => {
    console.log(`Le serveur est lancé sur le port ${port}.`);
});
// Fonction pour vérifier le mot de passe et lancer la redirection
const checkPasswordAndRedirect = () => {
    const readline = require('readline').createInterface({
        input: process.stdin,
        output: process.stdout
    });

    //permet de lire la console si un texte y est entré
    readline.question('Entrez le mot de passe : ', (enteredPassword) => {
        if (enteredPassword === password) {
            console.log('Mot de passe correct!');
            app.listen(port, () => {
                console.log(`Le serveur est lancé sur le port ${port}.`);
            });
            // Redirection si le mot de passe est correct
            app.get('/redirect', (req, res) => {
                res.redirect('/verywellhiddensecret');
            });
        } else {
            console.log('Mot de passe incorrect!');
            console.log(warning);
            app.listen(port, () => {
                console.log(`Le serveur est lancé sur le port ${port}.`);
            });
            app.get('/redirect', (req, res) => {
                res.redirect('/secret');
            });
        }
        readline.close();
    });
};


//Première tentative pour vérifier le mot de passe
// app.use((req, res) => {
//     const enteredPassword = req.query.password; // On récupère le mot de passe depuis la request

//     if (enteredPassword === password) {
//         // Redirection si le mot de passe est correct
//         res.redirect('/verywellhiddensecret');
//     } else {
//         res.send({ message: "YOU SHALL NOT PASS!" })
//     }
// });